import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import firebase from "./services/firebase";
import DashboardPlugin from "./plugins/dashboard-plugin";
import titleMixin from "./mixins/titleMixin";

import Default from "./views/layouts/Default.vue";
import HasSlidebar from "./views/layouts/HasSlidebar.vue";
import NoSlidebar from "./views/layouts/NoSlidebar.vue";

Vue.config.productionTip = false;
Vue.use(DashboardPlugin);
Vue.use(require("moment"));
Vue.mixin(titleMixin);
Vue.component("default-layout", Default);
Vue.component("has-slidebar-layout", HasSlidebar);
Vue.component("no-slidebar-layout", NoSlidebar);

firebase.auth().onAuthStateChanged((user) => {
  store.dispatch("fetchUser", user);
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
