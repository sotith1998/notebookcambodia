import Vue from "vue";
import VueRouter from "vue-router";
// import firebase from "@/services/firebase.js";
import "firebase/auth";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Pages/Home.vue"),
  },
  {
    path: "/technology",
    name: "Technology",
    component: () => import("../views/Pages/Technology.vue"),
  },
  {
    path: "/nation",
    name: "Nation",
    component: () => import("../views/Pages/Nation.vue"),
  },
  {
    path: "/national",
    name: "National",
    component: () => import("../views/Pages/National.vue"),
  },
  {
    path: "/sport",
    name: "Sport",
    component: () => import("../views/Pages/Sport.vue"),
  },
  {
    path: "/tourism",
    name: "Tourism",
    component: () => import("../views/Pages/Tourism.vue"),
  },
  {
    path: "/agricalture",
    name: "Agricalture",
    component: () => import("../views/Pages/Agriculture.vue"),
  },
  {
    path: "/general-knowlege",
    name: "GeneralKnowlege",
    component: () => import("../views/Pages/GeneralKnowlege.vue"),
  },
  {
    path: "/content/:id",
    name: "Content",
    component: () => import("../views/ContentPreview.vue"),
    meta: { requiresAuth: false },
  },
  {
    path: "/nc-admin",
    name: "Admin",
    component: () => import("../views/admin/index.vue"),
    meta: { requiresAuth: true, layout: "has-slidebar" },
  },
  {
    path: "/nc-admin/post/create",
    name: "PostCreate",
    component: () => import("../views/admin/post/create.vue"),
    meta: { requiresAuth: true, layout: "has-slidebar" },
  },
  {
    path: "/nc-admin/post/:id",
    name: "PostUpdate",
    component: () => import("../views/admin/post/update.vue"),
    meta: { requiresAuth: true, layout: "has-slidebar" },
  },
  {
    path: "/nc-admin/posts",
    name: "Posts",
    component: () => import("../views/admin/post/posts.vue"),
    meta: { requiresAuth: true, layout: "has-slidebar" },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/admin/auth/login.vue"),
    meta: { layout: "no-slidebar" },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/admin/auth/register.vue"),
    meta: { layout: "no-slidebar" },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  linkActiveClass: "active",
  routes,
});

// router.beforeEach((to, from, next) => {
//   const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
//   const isAuthenticated = firebase.auth().currentUser;
//   if (requiresAuth && !isAuthenticated) {
//     next("/login");
//   } else if (!requiresAuth && isAuthenticated) next("/nc-admin");
//   else next();
// });

// router.afterEach((to) => {
//   Vue.nextTick(() => {
//     document.title = to.meta.title ? to.meta.title : "default title";
//   });
// });

export default router;
