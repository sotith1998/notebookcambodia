import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyCOxlRkgNjvjvGDG6lDfFLhnAFuZtaRayM",
  authDomain: "notebookcambodia-5226e.firebaseapp.com",
  projectId: "notebookcambodia-5226e",
  storageBucket: "notebookcambodia-5226e.appspot.com",
  messagingSenderId: "526371568999",
  appId: "1:526371568999:web:b91129039d348a555983c2",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
