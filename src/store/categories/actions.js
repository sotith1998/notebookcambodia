import firebase from "../../services/firebase";
const TABLE = "categories_post";

export default {
  CATEGORIES: (context) => {
    return firebase
      .firestore()
      .collection(TABLE)
      .onSnapshot((snapshot) => {
        let items = [];
        snapshot.forEach((refDoc) => {
          items.push({
            id: refDoc.id,
            name: refDoc.data().name,
            status: refDoc.data().status,
          });
        });
        context.commit("CATEGORIES", items);
      });
  },
  NEW_CAT: (context, payload) => {
    return firebase
      .firestore()
      .collection(TABLE)
      .add({
        name: payload.name,
        status: payload.status,
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },
  FIND_CATEGORY: async (context, payload) => {
    let item = {};
    return await firebase
      .firestore()
      .collection(TABLE)
      .doc(payload.id)
      .get()
      .then((snapshot) => {
        item = {
          id: snapshot.id,
          name: snapshot.data().name,
          status: snapshot.data().status,
        };
        context.commit("FIND_CATEGORY", item);
      })
      .catch((err) => {
        return err;
      });
  },
};
