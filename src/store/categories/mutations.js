export default {
  CATEGORIES: (state, items) => {
    return (state.items = items);
  },
  FIND_CATEGORY: (state, item) => {
    return (state.item = item);
  },
};
