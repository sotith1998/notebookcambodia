import firebase from "../../services/firebase";
const TABLE = "posts";

export default {
  posts: (context) => {
    return firebase
      .firestore()
      .collection(TABLE)
      .orderBy("createdAt", "desc")
      .limit(10)
      .onSnapshot((snapshot) => {
        let items = [];
        snapshot.forEach((refDoc) => {
          items.push({
            id: refDoc.id,
            title: refDoc.data().title,
            category_id: refDoc.data().category_id,
            content: refDoc.data().content,
            isImportant: refDoc.data().isImportant,
            isPublic: refDoc.data().isPublic,
            fileURL: refDoc.data().fileURL,
            createdAt: refDoc.data().createdAt,
          });
        });
        context.commit("posts", items);
      });
  },
  newPost: (context, payload) => {
    return firebase
      .firestore()
      .collection(TABLE)
      .add({
        title: payload.title,
        category_id: payload.category_id,
        content: payload.content,
        createdAt: Date.now(),
        isImportant: payload.isImportant,
        isPublic: payload.isPublic,
        publicAt: null,
        fileURL: payload.fileURL,
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },
  updatePost: (context, payload) => {
    return firebase
      .firestore()
      .doc(payload.id)
      .update({
        title: payload.item.title,
        category_id: payload.item.category_id,
        content: payload.item.content,
        fileURL: payload.item.fileURL,
      })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },
  deletePost: (context, payload) => {
    return firebase
      .firestore()
      .doc(payload.id)
      .delete()
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  },
  findPost: async (context, payload) => {
    let item = {};
    return await firebase
      .firestore()
      .collection(TABLE)
      .doc(payload.id)
      .get()
      .then((snapshot) => {
        item = snapshot.data();
        context.commit("FIND_POST", item);
      })
      .catch((err) => {
        return err;
      });
  },
};
