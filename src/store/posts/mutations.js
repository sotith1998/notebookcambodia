export default {
  posts: (state, items) => {
    return (state.items = items);
  },
  FIND_POST: (state, item) => {
    return (state.item = item);
  },
};
