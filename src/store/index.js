import Vue from "vue";
import Vuex from "vuex";

import post from "./posts";
import category from "./categories";
import auth from "./auth";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    post,
    category,
    auth,
  },
});
export default store;
